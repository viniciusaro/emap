//
//  EViewController.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation

class EViewController: EAnnotatedViewController
{
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?)
    {
        super.init(nibName:nibNameOrNil, bundle:nibBundleOrNil)
        setUp()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        setUp()
    }
    
    func setUp()
    {
        // TODO: create logic to build proper address formatter
        self.addressFormatter = EPTAddressFormatter()
    }
}