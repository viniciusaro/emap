//
//  EHttpRequest.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import AFNetworking

class EHttpRequest {
    
    var cancelled = false
    
    func makeGETRequest(url:String, delegate: EHttpHelperDelegate, delay:NSTimeInterval)
    {
        // wait in background thread for the delay time
        EUtil.runInBackground
        {
            NSThread.sleepForTimeInterval(delay)
            
            // runs the request if not cancelled
            if !self.cancelled
            {
                EUtil.runOnMainThread
                {
                    self.makeGETRequest(url, delegate: delegate)
                }
            }
        }
    }
    
    func makeGETRequest(url:String, delegate: EHttpHelperDelegate)
    {
        var manager = AFHTTPRequestOperationManager()
        
        print("requesting new data...");
        delegate.didStartLoadingData()
        
        manager.GET(url, parameters: nil,
            success:
            { (requestOperation, responseObject) -> Void in
                
                println("done!");
                
                let dic = responseObject as! NSDictionary
                let response = dic["response"] as! NSDictionary
                
                // fires the delegate with response message (venues info)
                delegate.handleResult(response)
            },
            failure:
            { (requestOperation, error) -> Void in
                print("error = \(error)")
                
                // fires the delegate error messages
                delegate.handleError(error)
        });
    }
    
    func cancel()
    {
        self.cancelled = true;
    }
}