//
//  EPTAddressFormatter.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import CoreLocation

// formatter for pt-br
class EPTAddressFormatter: EAddressFormatter
{
    func formatAddress(placemark: CLPlacemark) -> String {
        return placemark.name
    }
}