//
//  EUtil.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import CoreLocation

extension Array
{    
    func containsLocation(location:CLLocationCoordinate2D) -> Bool
    {
        return self.filter
            {
                let coordinate = $0 as? CLLocationCoordinate2D
                
                if (coordinate!.latitude == location.latitude && coordinate!.longitude == location.longitude) {
                    return true
                }
                else {
                    return false
                }
                
            }.count > 0
    }
}

class EUtil {
    
    static func runInBackground(closure: () -> ())
    {
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            closure()
        }
    }
    
    static func runOnMainThread(closure: () -> ())
    {
        dispatch_async(dispatch_get_main_queue()) {
            closure()
        }
    }
}