//
//  HttpHelper.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import CoreLocation
import AFNetworking
import SwiftyJSON

protocol EHttpHelperDelegate {
    func handleResult(result:NSDictionary?)
    func didStartLoadingData()
    func handleError(error:NSError?)
}

class EHttpHelper
{
    static var request: EHttpRequest?
    
    static let client_id     = Constants.FOURSQUARE_CLIENT_ID
    static let client_secret = Constants.FOURSQUARE_CLIENT_SECRET
    static let version       = Constants.FOURSQUARE_API_VERSION
    
    static func makeFoursquareVenueRequestFor(
        location:CLLocationCoordinate2D,
        radius:Int,
        delegate: EHttpHelperDelegate,
        delay:NSTimeInterval)
    {
        let url = "https://api.foursquare.com/v2/venues/explore?ll=\(location.latitude),\(location.longitude)&client_id=\(client_id)&client_secret=\(client_secret)&v=\(version)&radius=\(radius)"
        
        // create a new request
        request = EHttpRequest()
        
        // make the request with delay
        request!.makeGETRequest(url, delegate: delegate, delay:delay)
    }
    
    static func cancel()
    {
        request?.cancel();
    }
}