//
//  ViewController.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 13/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

protocol EAddressFormatter {
    func formatAddress(placemark: CLPlacemark) -> String
}

class EAnnotatedViewController: EMapViewController, EHttpHelperDelegate
{
    
    var locations: Array<CLLocationCoordinate2D> = Array<CLLocationCoordinate2D>()
    var geocode = CLGeocoder()
    var addressFormatter: EAddressFormatter?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var addressLabel: UILabel!
    
    
    override func initializeMap(location: CLLocation)
    {
        super.initializeMap(location)
        requestDataForLocation(location.coordinate, delay:0)
        requestGeocodeData(location);
    }
    
    
    func requestGeocodeData(location: CLLocation)
    {
        geocode.reverseGeocodeLocation(location) { (placemarks:[AnyObject]!, error:NSError!) -> Void in
            let placemark = placemarks?[0] as! CLPlacemark
            self.addressLabel.text = self.addressFormatter?.formatAddress(placemark)
        }
    }
    
    
    func requestDataForLocation(location: CLLocationCoordinate2D, delay:NSTimeInterval) {
        EHttpHelper.makeFoursquareVenueRequestFor(location, radius:2000, delegate:self, delay:delay)
    }
    

    func mapView(mapView: MKMapView!, regionWillChangeAnimated animated: Bool) {
        EHttpHelper.cancel()
    }
    
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        requestDataForLocation(mapView.centerCoordinate, delay:1)
    }

    
    func didStartLoadingData() {
        activityIndicator?.startAnimating()
    }
    
    func handleResult(result: NSDictionary?)
    {
        activityIndicator?.stopAnimating()
        
        // parse foursquare info
        let groups = result?["groups"] as? NSArray
        let groupsInfo = groups?.objectAtIndex(0) as? NSDictionary
        let items = groupsInfo?["items"] as? NSArray
        
        println("\(items!.count) new items")
        
        // loads a new annotation for each new item
        for item in items!
        {
            // information for each venue
            let venue = item["venue"] as! NSDictionary
            
            var annotation = EAnnotation()
            
            annotation.venueInfo = venue
            
            if (!locations.containsLocation(annotation.coordinate))
            {
                locations.append(annotation.coordinate)
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    func handleError(error: NSError?)
    {
        // TODO: handle request error
        activityIndicator?.stopAnimating()
    }
}

