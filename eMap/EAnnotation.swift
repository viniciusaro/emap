//
//  EAnnotation.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import MapKit

class EAnnotation: MKPointAnnotation
{
    var venueInfo: NSDictionary?
    
    override var title: String! {
        get {
            return venueInfo?["name"] as! String
        }
        set {
            self.title = newValue
        }
    }
    
    override var coordinate: CLLocationCoordinate2D {
        get {
            let location = venueInfo?["location"] as! NSDictionary
            let latitudeString = location["lat"] as! NSNumber
            let longitudeString = location["lng"] as! NSNumber
            let latitude: CLLocationDegrees = latitudeString.doubleValue
            let longitude: CLLocationDegrees = longitudeString.doubleValue
            return CLLocationCoordinate2DMake(latitude, longitude)
        }
        set {
            self.coordinate = newValue
        }
    }
}