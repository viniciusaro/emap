//
//  MapViewController.swift
//  eMap
//
//  Created by Vinícius Rodrigues on 16/04/2015.
//  Copyright (c) 2015 eGenius. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class EMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate
{
    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    var didInitializeMap = false
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mapView.delegate = self
        initializeLocationHelpers()
    }
    
    func initializeLocationHelpers()
    {
        // request user authorization for location before
        // setting mapView.showsUserLocation = true
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        if status == CLAuthorizationStatus.AuthorizedAlways || status == CLAuthorizationStatus.AuthorizedWhenInUse {
            mapView.showsUserLocation = true
        }
    }
    
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!)
    {
        if !didInitializeMap {
            initializeMap(userLocation.location)
        }
    }
    
    
    func initializeMap(location: CLLocation)
    {
        didInitializeMap = true;
        centralizeMapAt(location.coordinate)
    }
    
    
    func centralizeMapAt(coordinate: CLLocationCoordinate2D)
    {
        var region = MKCoordinateRegionMakeWithDistance(coordinate, 2000, 2000)
        mapView.setRegion(region, animated: true)
    }
    
    
    func addAnnotationAt(coordinate: CLLocationCoordinate2D)
    {
        var pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = coordinate
        mapView.addAnnotation(pointAnnotation)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}